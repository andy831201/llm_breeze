# %%
from langchain_community.embeddings import OllamaEmbeddings
from langchain.document_loaders import TextLoader, PyPDFLoader
from langchain.text_splitter import  RecursiveCharacterTextSplitter
from langchain.vectorstores import Chroma
from langchain.chains import RetrievalQA
from langchain.memory import ConversationBufferMemory
from langchain_community.llms import Ollama

file_path = "/home/yang/Desktop/workspace/project/llm/breeze/static/VisionTransformer.pdf"
loader = file_path.endswith(".pdf") and PyPDFLoader(file_path) or TextLoader(file_path)

splitter = RecursiveCharacterTextSplitter(chunk_size=40960, chunk_overlap=1024) 
texts = loader.load_and_split(splitter)

embeddings = OllamaEmbeddings(model='ycchen/breeze-7b-instruct-v1_0')
# embeddings = OllamaEmbeddings(model='wangrongsheng/taiwanllm-7b-v2.1-chat')
# embeddings = OllamaEmbeddings(model='llama2-chinese')
vectorstore = Chroma.from_documents(texts, embeddings)

# %%
llm = Ollama(model='ycchen/breeze-7b-instruct-v1_0')
# llm = Ollama(model='wangrongsheng/taiwanllm-13b-v2.0-chat')
chat_memory = ConversationBufferMemory(memory_key="chat_history", return_messages=True)
memory = chat_memory

qa = RetrievalQA.from_chain_type(
    llm=llm,
    verbose=False,
    memory=memory,
    retriever=vectorstore.as_retriever(), 
)

# %%
question_list = [
"本文的作者是誰?",
"本文是否有提到在什麼資料集上訓練的?",
"本文有提到幾種模型?",
]

i = 0
while i < len(question_list):
    query = question_list[i]
    if not query:
        break
    result = qa.run(query)
    print('A:', result)
    print('-'*65)
    i += 1
# %%
