# %%
from langchain.prompts import (
    ChatPromptTemplate,
    MessagesPlaceholder,
    SystemMessagePromptTemplate,
    HumanMessagePromptTemplate,
)
from langchain.chains import LLMChain
from langchain.memory import ConversationBufferMemory
from langchain_community.llms import Ollama

llm = Ollama(model='ycchen/breeze-7b-instruct-v1_0')

# %%
prompt = ChatPromptTemplate(
    messages=[
        SystemMessagePromptTemplate.from_template(
            "You are a git teacher and make sure to answer in Chinese."
        ),
        MessagesPlaceholder(variable_name="chat_history"),
        HumanMessagePromptTemplate.from_template("{question}")
    ]
)

memory = ConversationBufferMemory(memory_key="chat_history",return_messages=True)

conversation = LLMChain(
    llm=llm,
    prompt=prompt,
    verbose=False,
    memory=memory,
)

# %%
data_list = [
"我需要上傳一個專案，請提供語法",
"我的remote_url為git@gitlab.con:A3465/first.git，請幫我完整remote語法"
]

i = 0
while i < len(data_list):
    data = data_list[i]
    answer = conversation({"question": data})
    print(answer["text"])
    print('-'*65)
    i += 1
# %%
