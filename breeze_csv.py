# %%
from langchain.prompts import (
    ChatPromptTemplate,
    MessagesPlaceholder,
    SystemMessagePromptTemplate,
    HumanMessagePromptTemplate,
)
from langchain.chains import LLMChain
from langchain.memory import ConversationBufferMemory
from langchain.text_splitter import  RecursiveCharacterTextSplitter
from langchain_community.llms import Ollama
import pandas as pd

llm = Ollama(model='ycchen/breeze-7b-instruct-v1_0', temperature=0)

# %%
df = pd.read_csv("./static/train.csv")
csv_string = df.to_csv(index=False)

splitter = RecursiveCharacterTextSplitter(chunk_size=512, chunk_overlap=0) 
texts = splitter.split_text(csv_string)

csv_prompt = []
for idx in range(len(texts)):
    if idx == 0:
        system_word = "read the following csv document:"
    else:
        system_word = ""
    temp_prompt = SystemMessagePromptTemplate.from_template(
        f"""
        {system_word}
        {texts[idx]}
        """
    )
    csv_prompt.append(temp_prompt)

prompt = ChatPromptTemplate(
    messages=[
        SystemMessagePromptTemplate.from_template(
            f"You are a data analysis engineer and make sure to answer in Chinese."
        ),
        *csv_prompt,
        MessagesPlaceholder(variable_name="chat_history"),
        HumanMessagePromptTemplate.from_template("{question}")
    ]
)

memory = ConversationBufferMemory(memory_key="chat_history",return_messages=True)

conversation = LLMChain(
    llm=llm,
    prompt=prompt,
    verbose=False,
    memory=memory,
)

# %%
data_list = [
"train_00028.png的label是多少?",
"label為5的有幾個?",
"幫我找出命名規則異常之ID",
"請統計label的個數並做成表格"
]

i = 0
while i < len(data_list):
    data = data_list[i]
    answer = conversation({"question": data})
    print(answer["text"])
    print('-'*65)
    i += 1
# %%
