# Breeze 模型介紹

## TODO list
1. fine-tune breeze
2. develop line chat bot with breeze llm

## experiments
read ViT paper

| 模型 | Q | A |
| --- | --- | --- |
| Chatgpt3.5 | 請簡單敘述一下此編文章 | 這篇文章探討了一個簡單的框架，用於對視覺表示進行對比學習。作者們通過在不同大小的數據集上進行預訓練並評估多個基準任務來了解每個模型的數據需求。在考慮模型預訓練的計算成本時，ViT表現非常優秀，在較低的預訓練成本下在大多數識別基準上達到了最先進的水平。最後，他們進行了一個小型實驗，使用自監督學習，並展示了自監督的ViT對未來具有潛力。|
| Chatgpt3.5 | 此篇文章的作者是誰 | 這篇文章的作者是Dmitry Lepikhin, HyoukJoong Lee, Yuanzhong Xu, Dehao Chen, Orhan Firat, Yanping Huang, Maxim Krikun, Noam Shazeer, 和 Zhifeng Chen。 |
| Chatgpt3.5 | 請問ViT-Huge模型的Layers有幾層 | ViT-Huge模型有32層 |
| Chatgpt3.5 | 那ViT-Base呢 | 是的，ViT-Base模型有12層 |

| 模型 | Q | A |
| --- | --- | --- |
| breeze | 請簡單敘述一下此編文章 | 這篇文章主要在討論如何使用可擴充的自然語言處理（NLP）Transformer 架構和其高效率實作，來處理二維影像。首先將二維影像轉換為一維序列的2D 網格，然後將這些序列輸入到標準 Transformer 模型中。文章還提到了一些相關研究，如 Ting Chen 等人在 ICML 2020b 發表的「A simple framework for contrastive learning of visual representations」、Yen-Chun Chen 等人在 ECCV 2020c 發表的「UNITER: UNiversal Image-TExt Representation Learning」，以及 Rewon Child 等人在 arXiv 2019 發表的「Generating long sequences with sparse transformers」。 |
| breeze | 此篇文章的作者是誰 | 這篇文章的作者是 Ting Chen, Simon Kornblith, Mohammad Norouzi, and Geoffrey E. Hinton. |
| breeze | 請問ViT-Huge模型的Layers有幾層 | ViT-Huge 模型有 32 層 |
| breeze | 那ViT-Base呢 | ViT-Base 模型有 12 層 |

## note
1. 增加system prompt後，反而效果下降  
    => 測試後，猜測是向量資料庫(Chroma)重複讀取造成影響