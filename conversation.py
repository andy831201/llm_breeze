# %%
from langchain.prompts import (
    ChatPromptTemplate,
    MessagesPlaceholder,
    SystemMessagePromptTemplate,
    HumanMessagePromptTemplate,
)
from langchain.chains import LLMChain
from langchain.memory import ConversationBufferMemory
from langchain_community.llms import Ollama

llm = Ollama(model='ycchen/breeze-7b-instruct-v1_0')

# %%
prompt = ChatPromptTemplate(
    messages=[
        SystemMessagePromptTemplate.from_template(
            "You are a data analysis engineer and make sure to answer in Chinese."
        ),
        MessagesPlaceholder(variable_name="chat_history"),
        HumanMessagePromptTemplate.from_template("{question}")
    ]
)

memory = ConversationBufferMemory(memory_key="chat_history",return_messages=True)

conversation = LLMChain(
    llm=llm,
    prompt=prompt,
    verbose=False,
    memory=memory,
)

# %%
data_list = [
"""
請閱讀以下csv內容,
ID,Label
train_00000.png,0
train_00001.png,1
train_00002.png,1
train_00003.png,5
train_00004.png,5
train_00005.png,5
train_00006.png,3
train_00007.png,0
train_00008.png,3
train_00009.png,5
train_00010.png,3
train_00011.png,5
train_00012.png,3
train_00013.png,3
train_00014.png,1
train_00015.png,1
train_00016.png,1
train_00017.png,1
train_00018.png,5
train_00019.png,1
train_00020.png,1
train_00021.png,1
train_00022.png,0
train_00023.png,5
train_00024.png,3
train_00025.png,1
train_00026.png,5
train_00027.png,5
train_00028.png,4
""",
"train_00028.png的label是多少?",
"label為5的有幾個?",
"是哪10個?請列舉出來"
]

i = 0
while i < len(data_list):
    data = data_list[i]
    answer = conversation({"question": data})
    print(answer["text"])
    print('-'*65)
    i += 1
# %%
