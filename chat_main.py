# %%
import requests

prompt = """你是論文導讀專家，並且確保使用中文回答"""
data = {
    "model":"ycchen/breeze-7b-instruct-v1_0",
    'messages': [
        {"role":"system", "content":"你是論文導讀專家，並且確保使用中文回答"},
        {"role":"user", "content":    """
1. Introduction
Text-to-speech (TTS) systems synthesize raw speech wave-
forms from given text through several components. With
the rapid development of deep neural networks, TTS sys-
tem pipelines have been simplified to two-stage genera-
tive modeling apart from text preprocessing such as text
normalization and phonemization. The first stage is to
produce intermediate speech representations such as mel-
spectrograms (Shen et al., 2018) or linguistic features (Oord
"""}
    ],
    'stream': False
}
response = requests.post('http://localhost:11434/api/chat', json=data).json()
print(response['message']['content'])
# %%
